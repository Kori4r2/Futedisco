// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class FUTEDISCO_API AMainMenuGameMode : public AGameModeBase {
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category="Futedisco")
	void HostChangeLevelForAllClients();
 
	UFUNCTION(BlueprintCallable, Category="Futedisco")
	virtual FString InitNewPlayer(
		APlayerController *NewPlayerController,
		const FUniqueNetIdRepl &UniqueId,
		const FString &Options,
		const FString &Portal
	) override;

	UFUNCTION(BlueprintImplementableEvent, Category="Futedisco")
	void UpdatePlayerData(APlayerController *NewPlayerController, FString const &displayName, FGuid const &uuid);

	UPROPERTY(EditAnywhere, Category = "Futedisco")
	TSoftObjectPtr<UWorld> PIEGameplayLevelRef;
};
