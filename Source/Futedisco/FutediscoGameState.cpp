// Fill out your copyright notice in the Description page of Project Settings.


#include "FutediscoGameState.h"


void AFutediscoGameState::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass) {
	
	if(CurrentWidget != nullptr) {
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}

	if(NewWidgetClass == nullptr) return;

	CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
	if(CurrentWidget != nullptr) {
		CurrentWidget->AddToViewport();
	} else {
		UE_LOG(LogTemp, Error, TEXT("Failed to create new widget"), *this->GetName());
	}
}

// Entry point, first scene
void AFutediscoGameState::BeginPlay() {
	ChangeMenuWidget(StartingWidgetClass);
	Super::BeginPlay();
}

