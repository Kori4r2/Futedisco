// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGameMode.h"
#include "Kismet/GameplayStatics.h"

#include "Engine/Engine.h"

void AMainMenuGameMode::BeginPlay() {
	AGameModeBase::bUseSeamlessTravel = true;
	Super::BeginPlay();
}


void AMainMenuGameMode::HostChangeLevelForAllClients(){
	static int i = 0;
	if(GIsEditor){
		GEngine->AddOnScreenDebugMessage(i++, 10.0, FColor::Cyan, "Cannot use server travel with PIE, for testing multiplayer use standalone player instead");
		UGameplayStatics::OpenLevelBySoftObjectPtr({}, PIEGameplayLevelRef, true, "");
	} else {
		GetWorld()->ServerTravel("/Game/Futedisco/Levels/ArenaLevel");
	}
}


// void AMainMenuGameMode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps ) const {
//     DOREPLIFETIME_CONDITION_NOTIFY(Class, Array, COND_REPNOTIFY_Always);
// }

FString AMainMenuGameMode::InitNewPlayer(
	APlayerController *NewPlayerController,
	const FUniqueNetIdRepl &UniqueId,
	const FString &Options,
	const FString &Portal
){
	FString displayName = UGameplayStatics::ParseOption(Options, TEXT("displayName"));
	FString playerUUIDString = UGameplayStatics::ParseOption(Options, TEXT("uuid"));
	FGuid *playerUUID = new FGuid();
	FGuid::Parse(playerUUIDString, *playerUUID);

	UpdatePlayerData(NewPlayerController, displayName, *playerUUID);

	// static int i = 0;
	// GEngine->AddOnScreenDebugMessage(i++, 60.0, FColor::Cyan, "displayName: " + displayName);
	// GEngine->AddOnScreenDebugMessage(i++, 60.0, FColor::Cyan, "playerUUID: " + playerUUID);
	return AGameModeBase::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
}

// 127.0.0.1