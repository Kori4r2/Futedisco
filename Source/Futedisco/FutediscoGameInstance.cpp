// Fill out your copyright notice in the Description page of Project Settings.


#include "FutediscoGameInstance.h"
 
void UFutediscoGameInstance::OnWorldChanged(UWorld *OldWorld, UWorld *NewWorld){
    Super::OnWorldChanged(OldWorld, NewWorld);
	OnWorldChangedForBlueprint(OldWorld, NewWorld);
}

void UFutediscoGameInstance::OnStart(){
    Super::OnStart();
	OnStartForBlueprint();
}

