// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerActionCollision.h"

#include "UObject/ConstructorHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SphereComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
APlayerActionCollision::APlayerActionCollision()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	CreateCollisionHitbox();

}

void APlayerActionCollision::CreateCollisionHitbox() {

}

// Called when the game starts or when spawned
void APlayerActionCollision::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerActionCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerActionCollision::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

