// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FutediscoGameState.generated.h"

/**
 * 
 */
UCLASS()
class FUTEDISCO_API AFutediscoGameState : public AGameStateBase {
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Futedisco", BlueprintCosmetic)
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Futedisco")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	UPROPERTY(BlueprintReadOnly, Category = "Futedisco")
	UUserWidget *CurrentWidget;
};
