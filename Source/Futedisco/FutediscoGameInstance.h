// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FutediscoGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class FUTEDISCO_API UFutediscoGameInstance : public UGameInstance{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintImplementableEvent, Category="Futedisco")
	void OnWorldChangedForBlueprint(UWorld *OldWorld, UWorld *NewWorld);

	virtual void OnWorldChanged(UWorld *OldWorld, UWorld *NewWorld) override;

	UFUNCTION(BlueprintImplementableEvent, Category="Futedisco")
	void OnStartForBlueprint();

	virtual void OnStart() override;
};
